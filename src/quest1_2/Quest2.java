package quest1_2;

import java.util.LinkedList;
import java.util.List;

public class Quest2 {
    public static List<Integer> fibbonachy(int limit) {
        List<Integer> result = new LinkedList<>();
        result.add(0);
        result.add(1);

        for (int i = 1; true; i++) {
            if (result.get(i) >= limit) {
                result.remove(i);
                break;
            }
            result.add(result.get(i - 1) + result.get(i));
        }
        return result;
    }
}
