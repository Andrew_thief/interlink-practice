package quest1_2;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Quest1 {
    public static Map<String, Integer> countInput(String input) {
        return Arrays.stream(input.split(" "))
                .map(String::toLowerCase)
                .collect(Collectors.groupingBy(
                        Function.identity(),
                        Collectors.summingInt(e -> 1)
                ));
    }
}
