import quest3.Menu;

import java.util.Scanner;

import static quest1_2.Quest1.countInput;
import static quest1_2.Quest2.fibbonachy;

public class Main {
    public static void main(String[] args) {
        System.out.println(countInput("Andrew andrew Andrew interlink"));
        System.out.println(fibbonachy(10));

        Scanner scanner = new Scanner(System.in);
        Menu menu = new Menu(scanner);
        menu.startApp();
    }
}
