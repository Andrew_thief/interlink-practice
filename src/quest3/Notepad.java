package quest3;

import java.util.List;

public class Notepad {
    private List<ToDoList> toDoLists;

    public void deleteToDoList(int number){
        if(!toDoLists.isEmpty()){
            toDoLists.remove(number);
        }
    }

    public void createNewList(String name){
        toDoLists.add(new ToDoList(name));
    }

    public  void renameList(int number, String name){
        if(number <= toDoLists.size()){
            toDoLists.get(number).setNameOfList(name);
        }
    }

    public ToDoList getToDoList(int num){
        return toDoLists.get(num);
    }

    public List<ToDoList> getLists() {
        return toDoLists;
    }

}
