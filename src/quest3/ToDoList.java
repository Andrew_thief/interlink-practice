package quest3;

import java.util.ArrayList;
import java.util.List;

public class ToDoList {

    private String nameOfList;
    private List<Task> listToDo;

    public ToDoList(String nameOfList) {
        this.nameOfList = nameOfList;
        this.listToDo = new ArrayList<>();
    }

    public void setTaskAsDone(int number, boolean activity) {
        if(number <= listToDo.size()){
        listToDo.get(number).setDone(activity);}
    }

    public void setNameOfTask(int number, String newName){
        if(number <= listToDo.size()){
            listToDo.get(number).setName(newName);
        }
    }

    public void clearAllTasks() {
        listToDo.clear();
    }

    public void setNameOfList(String nameOfList) {
        this.nameOfList = nameOfList;
    }

    public String getNameOfList() {
        return nameOfList;
    }

    public List<Task> getListToDo() {
        return listToDo;
    }
}
