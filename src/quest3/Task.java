package quest3;

public class Task {

    private boolean done;
    private String  name;

    public Task(boolean done, String name) {
        this.done = done;
        this.name = name;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public void setName(String name) {
        this.name = name;
    }
}
