package quest3;

import java.util.Scanner;

public class Menu {
    private Notepad notepad;
    private Scanner scanner;

    public Menu(Scanner scanner) {
        this.notepad = new Notepad();
        this.scanner = scanner;
    }

    public void startApp() {
        while (true) {
            showMenu();
        }
    }

    public void showMenu() {
        System.out.println("\nChoose operation" +
                "\nWrite \"help\" to show comands" +
                "\n=====================");

        getOperation(scanner.nextLine());
    }

    private void getOperation(String input) {
        if (input.toLowerCase().contains("delete")) {
            deleteList();
        } else if (input.contains("create")) {
            System.out.println("Write name of List");
            notepad.createNewList(scanner.nextLine());
        } else if (input.contains("rename")) {
            renameList();
        } else if (input.contains("choose")) {

        } else if (input.contains("help")) {
            System.out.println("delete" +
                    "create" +
                    "rename" +
                    "choose" +
                    "help" +
                    "exit");
        } else if (input.contains("exit")) {
            exitProgramm();
        } else {
            System.out.println("command not found");
        }
    }
    private void deleteList(){
        showLists();
        int num = scanner.nextInt();
        if(notepad.getLists().contains(num)){
            notepad.deleteToDoList(num);
        } else{
            System.out.println("list with this number is not exist");
        }
    }

    private void chooseList(){
        showLists();
        int num = scanner.nextInt();
        if(notepad.getLists().contains(num)){
           operationWithList(notepad.getToDoList(num));
        } else{
            System.out.println("list with this number is not exist");
        }
    }

    private void operationWithList(ToDoList toDoList) {

    }

    private void renameList(){
        showLists();
        int num = scanner.nextInt();
        if(notepad.getLists().contains(num)){
            System.out.print("Write new name: ");
            notepad.renameList(num, scanner.nextLine());
        } else{
            System.out.println("list with this number is not exist");
        }
    }

    private void exitProgramm() {
        System.exit(0);
    }

    private void showLists() {
        System.out.println("choose list\n");

        if (notepad.getLists().isEmpty()) {
            System.out.println("notepad is empty");
            return;
        }
        int i = 1;
        for (ToDoList toDoList : notepad.getLists()) {
            System.out.println(i + ". " + toDoList.getNameOfList());
            i++;
        }
    }

}
